#!/usr/bin/env python3
''' This module is a wrapper for Linux's mount function. There is no need for you to use it as 
you have Linux's mount function. Although this as a script is useless the class is useful
But if you want to use it as a mounter script and not a class read on..

Script will mount your drive. Script expects two keyword arguments.

        1: device = the path to the device:drive you want to mount. 
            ->  /dev/(device)
            --> device=/dev/sdc2

        2: mountpoint = the path you want the device:drive to be mounted into. Directory 
                        will be created if it does not exist.
            ->  /mount/$USER/(mountpoint)
            --> mountpoint=/media/$USER/thisIsHowYouDoIt

    Example: 
        sudo python3 Mounter.py device=/dev/sdc2 mountpoint=/media/$USER/thisIsHowYouDoIt

Script will unmount your drive. Script expects one keyword arguments and one argument 
    variable.

        1: mountpoint = the path you want the device:drive to be mounted into. Directory 
                        will be created if it does not exist.
            ->  /mount/$USER/(mountpoint)
            --> mountpoint=/media/$USER/thisIsHowYouDoIt

        2: -u ->  Flag mountpoint for unmount. Pass as single argument not keyword argument/
              --> -u
    Example: 
        sudo python3 Mounter.py device=/dev/sdc2 mountpoint=/media/$USER/thisIsHowYouDoIt -u

If you want to see licensing info you can pass 'license' as an arg. Will print the whole legal 
thing. ( Bash: Mounter.py 'license' or Python: import Mounter; Mounter('license') )

    Here's the basics:
    
        Mounter.py, a module that will mount your drives for you.
        Copyright (C) 2019  Austin Byron austin@3ch0peak.com

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

from sys import argv as sArgv
from sys import exit as sExit
from os.path import exists
from os import makedirs
from os import rmdir
import re
import subprocess as subP
from psutil import disk_partitions

class Mounter():
    ''' This class will mount/unmount your drive. If not passed --no-make in *arg the class expects
    two keyword arguments.

        1: device = the path to the device:drive you want to mount. 
            -> /dev/(device)

        2: mountpoint = the path you want the device:drive to be mounted into. Directory 
                        will be created if it does not exist.
            -> /mount/$USER/(mountpoint)

    If passed the --no-make will not check for (device, mountpoint) in karg. For manual use.
        Example:
            from time import sleep
            ## Initiate the class as variable.
            mnt = Mounter('--no-make')

            ## print out the devices
            mnt.lsblk('-p')

            ## make mountpoint directory string.
            mp = '/media/$USER/wellIhope'

            ## We have to make the directory before we can mount into it.
            makedirs(mp)

            ## We can now mount the volume.
            mnt.mount(device='/dev/sdc2', mountpoint=mp)

            ## print out only mounted volumes
            mnt.lsMounted('-p')

            ## do your thing.
            sleep(1.5*60)

            ## do your thing. Done? Good, now we can unmount.
            mnt.umount(mountpoint=mp)

    Mounter class comes with two helper methods.

        1: lsblk(), If passed the '-p' flag method will print the output of Linux command lsblk. 
            useful to find devices:drives to mount.
                Example:
                    ## list all available devices:drives
                    mnt.lsblk('-p')

        2: lsMounted(), If passed the '-p' flag method will print all relevant devices that are mounted.
            useful to verify your device:drive has been mounted.
                Example:
                    ## list all mounted devices:drives
                        mnt.lsMounted('-p')

    Mounter.py Copyright (C) 2019  Austin Byron austin@3ch0peak.com
    This program comes with ABSOLUTELY NO WARRANTY; for details pass 
    'license' as an argument to the program ( Mounter.py 'license' ).
    This is free software, and you are welcome to redistribute it
    under certain conditions; pass 'license' as arg for details.
    You can also find a current license info here:
    https://github.com/tetoNidan/mounter/blob/master/LICENSE.md
        '''

    def __init__(self, *arg, **karg):
        ''' To make or to --no-make, is that even a question? You can also print license info
        license in arg. '''
        if 'license' in arg:
            license()

        if '--no-make' not in arg:
            if '-u' not in arg and 'device' not in karg.keys():
                sExit('No device specified.')
            elif '-u' not in arg and not exists(karg['device']):
                sExit('Device not found')
            elif 'mountpoint' not in karg.keys():
                sExit('No mountpoint given.')
            elif not exists(karg['mountpoint']):
                makedirs(karg['mountpoint'])

            if '-u' in arg:
                self.umount(**karg)
            else:
                self.mount(**karg)

    def mount(self, **karg):
        ''' Mount a drive. Takes a device to mount and a mount point.

            1: device = the path to the device:drive you want to mount. 
                -> /dev/(device)

            2: mountpoint = the path you want the device:drive to be mounted into. Directory 
                            will be created if it does not exist.
                -> /mount/$USER/(mountpoint)'''

        cmd = 'sudo mount {} {}'.format(karg['device'], karg['mountpoint'])
        proc = subP.Popen(cmd.split(' '), stdout=subP.PIPE, stderr=subP.PIPE, stdin=subP.PIPE)
        stdErr, stdOut = proc.communicate()

        stdErr = stdErr.decode()
        stdOut = stdOut.decode()

        if stdErr != '':
            print(stdErr.strip())
        elif stdOut != '':
            print(stdOut.strip())
        elif karg['device'] in [x.device for x in self.lsMounted()]:
            print('{} has been mounted.'.format(karg['device']))

    def umount(self, **karg):
        ''' Unmount the specified volume. 

            1: mountpoint = the path you want the device:drive to be mounted into. Directory 
                            will be created if it does not exist.
                -> /mount/$USER/(mountpoint)'''

        cmd = 'sudo umount {}'.format(karg['mountpoint'])
        proc = subP.Popen(cmd.split(' '), stdout=subP.PIPE, stderr=subP.PIPE)
        stdErr, stdOut = proc.communicate()

        stdErr = stdErr.decode()
        stdOut = stdOut.decode()

        if stdErr != '':
            print(stdErr.strip())
        elif stdOut != '':
            print(stdOut.strip())
        elif karg['mountpoint'] not in [x.mountpoint for x in self.lsMounted()]:
            print('{} has been unmounted.'.format(karg['mountpoint']))
            rmdir(karg['mountpoint'])

    def lsblk(self, *args):
        ''' This function runs lsblk which list all of your drives. Then it removes 
            all the loops.

            If passed the '-p' flag method will print the output of Linux command lsblk. 
            useful to find devices:drives to mount.
                Example:
                    ## list all available devices:drives
                    mnt.lsblk('-p')
            Else will return and not print.'''

        proc = subP.Popen(['lsblk'], stdout=subP.PIPE, stderr=subP.PIPE)
        procCom = proc.communicate()
        findLoop = re.compile(r"loop.*\n")

        if True in [True for x in ['-p', '--print'] if x in args]:
            print(re.sub(findLoop, '', procCom[0].decode('UTF-8').strip()))
        else:
            return re.sub(findLoop, '', procCom[0].decode('UTF-8').strip())

    def lsMounted(self, *args):
        ''' This function lists the mounted drives. No loops included. 

            If passed the '-p' flag method will print all relevant devices that are mounted.
            useful to verify your device:drive has been mounted.
                Example:
                    ## list all mounted devices:drives
                    mnt.lsMounted('-p')
            Else will return a psutil.process_iter() object and will not print.'''

        mounted = [x for x in disk_partitions() if 'loop' not in x.device]

        if True in [True for x in ['-p', '--print'] if x in args]:
            for mt in mounted:
                # print mt
                ret_msg = f'Device: {mt.device} | Mount Point: {mt.mountpoint} | File System Type: {mt.fstype} '
                print(ret_msg)
        else:
            return mounted


def license():

    lic = 'LICENSE.md'
    fOpen = open(lic, 'r')
    legal = fOpen.read()
    fOpen.close()
    print(legal)

def mkArgs(dlArgs):
    ''' split the sys.argv args for testing '''
    if len(dlArgs) > 1:
        lt, dt = [], {}
        for ar in dlArgs[1:]:
            if '=' in ar:
                a = ar.split('=')
                if a[1] in ['false', 'False']:
                    dt[a[0]] = False
                elif a[1] in ['true', 'True']:
                    dt[a[0]] = True
                else:
                    dt[a[0]] = a[1]
            else:
                lt.append(ar)
        return (lt, dt)
    else:
        return None

if __name__ == '__main__':
    ## split the sys.argv args for testing
    args = mkArgs(sArgv)

    ## Tests I have run to make sure the program works as intended. 

    if args is None:
        from time import sleep
        ## Initiate the class as variable.
        mnt = Mounter('--no-make')

        ## print out the devices
        mnt.lsblk('-p')

        ## make mountpoint directory string.
        mp = '/media/$USER/wellIhope'

        ## We have to make the directory before we can mount into it.
        makedirs(mp)

        ## We can now mount the volume.
        mnt.mount(device='/dev/sdc2', mountpoint=mp)

        ## print out only mounted volumes
        mnt.lsMounted('-p')

        sleep(1.5*60)

        ## do your thing. Done? Good, now we can unmount.
        mnt.umount(mountpoint=mp)

    elif args[0] == [] and args[1] != {}:
        Mounter(**args[1])
    elif args[0] != [] and args[1] == {}:
        Mounter(*args[0])
    else:
        Mounter(*args[0], **args[1])
